# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'user/get_name' do
  it 'should have name input' do
    render
    expect(rendered).to match(/name/)
  end

  it 'should have Say Hello button' do
    render
    expect(rendered).to match(/Say Hello/)
  end

  it 'should have a form that submits to user/wish_hello' do
    render
    expect(rendered).to match %r{user/wish_hello}
  end
end
