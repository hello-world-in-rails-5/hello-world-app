# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UserController do
  describe '#get_name' do
    it 'must respond to UserController#get_name' do
      get :get_name
      expect(response.status).to eq(200)
    end
  end

  describe '#wish_hello' do
    render_views
    it 'must assign @name to params[:name]' do
      post :wish_hello, params: { name: 'World' }
      assigns(:name).should == 'World'
    end

    it 'should wish Hello <name>' do
      post :wish_hello, params: { name: 'World' }
      expect(response.body.match(/Hello World/)).to be_truthy
    end
  end
end
