# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'routes for user', type: :routing do
  it 'must have GET /user/get_name/' do
    expect(get: '/user/get_name/').to route_to(
      controller: 'user',
      action: 'get_name'
    )
  end

  it 'must have POST /user/wish_hello/' do
    expect(post: '/user/wish_hello/').to route_to(
      controller: 'user',
      action: 'wish_hello'
    )
  end
end
