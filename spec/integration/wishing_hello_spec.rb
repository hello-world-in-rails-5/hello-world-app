# frozen_string_literal: true

require 'rails_helper'

feature 'Getting wished Hello' do
  scenario 'Fillin the form, submit, get wished Hello <name>' do
    visit '/user/get_name'
    fill_in 'Name', with: 'World'
    click_button 'Say Hello'
    expect(page).to have_content 'Hello World'
  end
end
