# README

A sample app to explain how to do testing with rails.

# The Spec

## issue_1 Wish Me Hello
  As a user I need a web page where people can enter their name in a text box, and press a button that says "Say Hello", then the user should be greeted with a hello.

    A page must be present that will receive the user name

      A route GET /user/get_name must be there to get user name

      A page must appear in the route with a input text box to get name

      The page must contain a button called "Say Hello"


    A page must be present that shows "Hello name"

      A route POST user/wish_hello must exist to wish user hello

      The page must receive a parameter called :name

      The page must show "Hello World" when params[:name] is set to World
