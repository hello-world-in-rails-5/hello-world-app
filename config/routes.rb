Rails.application.routes.draw do
  get 'user/get_name', to: 'user#get_name'
  post 'user/wish_hello', to: 'user#wish_hello'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
