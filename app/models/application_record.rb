# frozen_string_literal: true

##
# Connects with the DB
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
